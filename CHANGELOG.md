# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.4] - 2020-01-03

### Changed
- update_homebrew.sh

## [1.0.3] - 2019-04-30

### Changed
- update_homebrew.sh

## [1.0.2] - 2019-04-04

### Changed
- install_script.sh
- update_homebrew.sh

## [1.0.1] - 2017-10-28

### Changed
- install_script.sh

## [1.0.0] - 2017-10-23

### Added
- .gitignore
- CHANGELOG.md
- README.md
- local.updatehomebrew.plist
- update_homebrew.sh
- install_script.sh
