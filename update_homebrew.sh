#!/bin/bash

LOGFILE="$HOME/Library/Logs/brew_update.log"
[[ -z ${BREW_EXEC+x} ]] && BREW_EXEC="/usr/local/bin/brew"

echo "

-------------------------------------------------------------------------------
>>>>> Script started at [`date`] >>>>>
-------------------------------------------------------------------------------\
" |  tee -a $LOGFILE

$BREW_EXEC update 2>&1 |  tee -a $LOGFILE
$BREW_EXEC upgrade 2>&1 |  tee -a $LOGFILE
$BREW_EXEC cask upgrade 2>&1 |  tee -a $LOGFILE
$BREW_EXEC cleanup 2>&1 |  tee -a $LOGFILE

echo "\
-------------------------------------------------------------------------------
<<<<< Script ended at [`date`] <<<<<
-------------------------------------------------------------------------------\
" |  tee -a $LOGFILE
